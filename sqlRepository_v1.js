const sql = require('mssql');
const _ = require('lodash');
const config = require('./dbconfig.json');

module.exports = {
    getPaging: async function (tableName,
        fieldNames,
        pageNumber,
        pageSize,
        sortBy,
        sortOrder,
        whereFields,
        searchText,
        searchFieldNames) {
        try {
            let pool = await connect();
            var request = new sql.Request(pool);
            request.input('tableName', sql.VarChar, tableName);
            request.input('fieldNames', sql.VarChar, fieldNames);
            request.input('pageNumber', sql.Int, pageNumber);
            request.input('pageSize', sql.Int, pageSize);
            request.input('sortBy', sql.VarChar, sortBy);
            request.input('sortOrder', sql.VarChar, sortOrder);

            // Setup Where Clause
            var sqlWhereFields = " 1 = 1";
            if (whereFields && whereFields.length > 0) {
                _.forEach(whereFields, function (field) {
               //     console.log(field);
                    sqlWhereFields += " AND " + field.name + " " + field.compare + "@" + field.name;
                    request.input(field.name, field.dataType, field.value);
                });

            }
            request.input("sqlWhereFields", sql.VarChar, sqlWhereFields);
          //  console.log("sqlWhereFields:", sqlWhereFields);

            // Set Up Order By
            var orderBy = "";
            if (sortBy !== "") {
                orderBy = "order by " + sortBy;
                if (sortOrder) {
                    orderBy += " DESC "
                }
            }
            request.input('orderBy', sql.VarChar, orderBy);
            var searchWhereString = await setSearchBy(searchText, searchFieldNames);
            if(searchWhereString)
            {
                _.forEach(searchWhereString.Parameters, function (p) {
                    sqlWhereFields += searchWhereString.SearchTextString;
                    request.input(p.fieldName, sql.VarChar, p.value);
                });
            }
            // Build query
            var q = `select overall_count = COUNT(*) OVER(), ${fieldNames} from ${tableName} where ${sqlWhereFields} ${orderBy} OFFSET (${pageNumber}-1)*${pageSize} ROWS FETCH NEXT 10 ROWS ONLY  `;
            //console.log("query:", q);
            const result = await request.query(q);
            return result;
        }
        catch (err) {
            console.log("Error:", err.message);
            return null;
        }
    },
    getSingle: async function (tableName,
        fieldNames,
        publicID) {
        try {
            let pool = await connect();
            var request = new sql.Request(pool);
            request.input('tableName', sql.VarChar, tableName);
            request.input('fieldNames', sql.VarChar, fieldNames);
            request.input('publicID', sql.VarChar, publicID);


            // Build query
            var q = `select ${fieldNames} from ${tableName} where publicID=@publicID`;
            console.log("query:", q);
            const result = await request.query(q);
            return result;
        }
        catch (err) {
            console.log("Error:", err.message);
            return null;
        }
    },
    getAll: async function (tableName,
        fieldNames) {
        try {
            let pool = await connect();
            var request = new sql.Request(pool);
            request.input('tableName', sql.VarChar, tableName);
            request.input('fieldNames', sql.VarChar, fieldNames);

            // Build query
            var q = `select ${fieldNames} from ${tableName}`;
            console.log("query:", q);
            const result = await request.query(q);
            return result;
        }
        catch (err) {
            console.log("Error:", err.message);
            return null;
        }
    },
    add: async function (tableName,
        entity) {
        try {
            var properties = Object.getOwnPropertyNames(entity);
            _.forEach(properties, function (p) {
                console.log("Field:",p);
            });
      return null;
            let pool = await connect();
            var request = new sql.Request(pool);
            request.input('tableName', sql.VarChar, tableName);
            request.input('fieldNames', sql.VarChar, fieldNames);

            // Build query
            var q = `select ${fieldNames} from ${tableName}`;
            console.log("query:", q);
            const result = await request.query(q);
            return result;
        }
        catch (err) {
            console.log("Error:", err.message);
            return null;
        }
    }
};

async function connect() {
    try {
        console.log("Connecting...", config);
        return await sql.connect(config);

    } catch (err) {
        console.log("SQL connect Error:", err.message);
        return null;
    }
}

async function setSearchBy(searchText, SearchFieldString) {
    if (searchText) {
        var Parameters=[];
        searchText = searchText.toLowerCase();
        //console.log('searchText:', searchText);
        var SearchTextString = "";
        try {

            fields = SearchFieldString;
            var searchStrings = searchText.split(' ');
            _.forEach(fields, function (f) {
                if (SearchTextString.length == 0) SearchTextString = " AND ( ( ";
                else SearchTextString += " OR ( ";

                searchstringcounter = 1;
                var FieldSearchText = "";
                _.forEach(searchStrings, function (s) {
                    var paramName = "@searchstring_" + searchstringcounter;
                    var fieldName = "searchstring_" + searchstringcounter;;
                    Parameters.push({'fieldName':fieldName,'value': s});

                    if (FieldSearchText.length > 0) FieldSearchText += " AND ";

                    FieldSearchText += `${f} like '%'+${paramName}+'%'`;
                    searchstringcounter++;
                });
                if (FieldSearchText.length > 0) {
                    FieldSearchText += " )";
                    SearchTextString += FieldSearchText;
                }
            });
            //console.log("Parameters:",Parameters);
        }
        catch (err) { }

        if (SearchTextString.length > 0) {
            SearchTextString += " ) ";
        //    console.log("SearchTextString:", SearchTextString);
            var obj  = {};
            obj.SearchTextString = SearchTextString;
            obj.Parameters = Parameters;
            return obj;
        }
        return null;
    }
}
