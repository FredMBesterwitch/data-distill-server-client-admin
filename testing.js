var repository = require('./sqlRepositoryRunTime');
const sql = require('mssql');
const _ = require('lodash');
// const bytenode = require('bytenode');


(async () => {
    try {
        // test for getPaging
        // var whereFieldNames = [];
        // var searchFieldNames = [];
        // whereFieldNames.push({ 'name': 'TableType', 'dataType': sql.Int, 'value': 1, 'compare': '=' });
        // whereFieldNames.push({ 'name': 'Active', 'dataType': sql.Bit, 'value': 1, 'compare': '=' });
        // searchFieldNames.push('Description');
        //
        // var result = await repository.getPaging("LookUpTables", "*", 1, 10, "Description", 0, whereFieldNames, "",searchFieldNames);
        // //console.log("result:", result);
        // if (result) {
        //     _.forEach(result.recordsets, function (row) {
        //         console.log(row);
        //     });
        // }
        // test for getSingle
        // var result = await repository.getSingle("Company", "*", '2');
        // console.log(result.recordset);
        // if (result) {
        //     _.forEach(result.recordsets, function (row) {
        //         console.log(row);
        //     });
        // }
        // test for getAll
        // var result = await repository.getAll("company", "*");
        // if (result) {
        //     _.forEach(result.recordsets, function (row) {
        //         console.log(row);
        //     });
        // }
        // // Add
        // var obj = {};

        // obj.CompanyName = "Company 28"
        //
        // obj = {
        //  "ID": 'jeyficw',
        //  "publicID": undefined,
        //  "Document_ID": '8',
        //  "Parent_ID": undefined,
        //  "ColumnID": undefined,
        //  "FieldName": 'jeyficw',
        //  "ShowLayout": true,
        //  "Caption": 'caption 1',
        //  "DataType": '17',
        //  "DateValue": 'DD/MM/YY',
        //  "MinValue": 0,
        //  "MaxValue": 999999,
        //  "DefaultValue": '',
        //  "EmptyValue": false,
        //  "x": 0,
        //  "y": 0,
        //  "b": 0,
        //  "r": 0,
        //  "top": 85,
        //  "left": 83,
        //  "width": 399,
        //  "height": 259
        // };
        // obj.publicID = "9CE332EA-499B-4480-86EF-A0B206B39EE3";
        // obj.Company_ID="10";
        // obj.ShowDocument=1;
        // obj.DocumentName="Company 20 Document_3";
        // obj.DocumentDescription="Company 20 Document_3 Description";
        // obj.InputDocumentType="9";
        // obj.InputMethod=9;
        // obj.OutputDocumentType=9;
        // obj.OutputMethod="9";
        // obj.InputSecurityType=9
        // obj.OutputSecurityType=9
        // obj.IncomingEmail="uf@gmail.com";
        // obj.OutGoingEmail="ug@gmail.com";
        //
        //
        // var result = await repository.add("Documents",obj);
        // console.log("add result!!", result);
        // if (result) {
        //     _.forEach(result.recordsets, function (row) {
        //         console.log("happy", row);
        //     });
        // }

        // Update
        // var obj = {};
        //
        // obj = {
        //   "ID": '101',
        //   "publicID": '85B3B296-3B3B-41FC-BB18-FD05576D053B',
        //   "Document_ID": '20',
        //   "PageNo": 2,
        //   "Parent_ID": null,
        //   "ColumnID": null,
        //   "FieldName": '',
        //   "ShowLayout": true,
        //   "Caption": 'caption 2 com30 doc1',
        //   "DataType": 19,
        //   "DateValue": 'DD/MM/YY',
        //   "MinValue": 10,
        //   "MaxValue": 999999,
        //   "DefaultValue": '',
        //   "EmptyValue": false,
        //   "TablePosition": null,
        //   "TableTopValue": null,
        //   "TableBottomValue": null,
        //   "x": 81,
        //   "y": 83,
        //   "b": 304,
        //   "r": 355,
        //   "top_int": 112,
        //   "left_int": 110,
        //   "width": 368,
        //   "height": 296,
        //   "tabularFieldArray": []
        // }
        // obj.publicID = "6D08F639-42B4-40AA-8773-70D8FEBA8467";
        // obj.Company_ID='2';
        // obj.ShowDocument= false;
        // obj.DocumentName="U Document_1";
        // obj.DocumentDescription="U Document_1 Description";
        // obj.InputDocumentType="9";
        // obj.InputMethod=9;
        // obj.OutputDocumentType=9;
        // obj.OutputMethod="9";
        // obj.InputSecurityType=9
        // obj.OutputSecurityType=9
        // obj.IncomingEmail="uf@gmail.com";
        // obj.OutGoingEmail="ug@gmail.com";


        // var result = await repository.update("Layouts",obj);
        // if (result) {
        //     _.forEach(result.recordsets, function (row) {
        //         console.log("update record", row);
        //     });
        // }

        var result = await repository.delete("Layouts","AC82B4C1-02F9-4BF8-BC09-06836279778F");
        // var result1 = await repository.delete("Layouts","E8CB41EB-9DDF-4DD6-B024-F2B578D144CA");
        console.log("result", result);
        if (result) {
            _.forEach(result.recordsets, function (row) {
                console.log(row);
            });
        }

         // test for executs
        //  var params = [];
        //  params.push({ 'name': 'TableType', 'dataType': sql.Int, 'value': 1});
        //  result = await repository.execute("SELECT * FROM LookUpTables WHERE TableType=@TableType", params);
        //  if (result) {
        //     _.forEach(result.recordsets, function (row) {
        //         console.log(row);
        //     });
        // }
    } catch (e) {
        console.log("Error:", e);
    }
})();
