// Version 2019.11.14 01

const express = require('express');
var cors = require('cors');
const bodyParser = require('body-parser')
const app = express();
const port = 8000;
const db = require("./database.js");
var repository = require('./sqlRepository');
const sql = require('mssql');
const _ = require('lodash');

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.options('*', cors());
app.use(function (req, res, next) {
 res.header("Access-Control-Allow-Origin", "*");
 res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
 next();
});

app.get('/', function (req, res) {
  res.send('Hello World')
})

app.get('/layout/getall/:tablename/:page/:pagesize/:sortby/:sortorder/:showAll/:searchText?', async (req, res) => {

  var sortorder = false
  if (req.params.sortorder == 'true') sortorder = true;
  var whereFieldNames = [];
  var searchFieldNames = [];
  searchFieldNames.push('CompanyName');
  if (Number(req.params.showAll) == 1) whereFieldNames.push({ 'name': 'Active', 'dataType': sql.Bit, 'value': 1, 'compare': '=' });
  var data = await db.getAll(req.params.tablename, req.params.page, req.params.pagesize, req.params.sortby, sortorder, req.params.showAll, whereFieldNames, req.params.searchText, searchFieldNames);
  var returnObj = {};
  returnObj.Data = data;
  returnObj.RecordCount = 0;
  if (data && data.length > 0) {
    returnObj.RecordCount = data[0].overall_count;
  }

  console.log("/layout/getAll ==>", returnObj);
  return res.status(200).send(returnObj);
});

app.get('/layout/getitem/:tablename/:fieldNames/:publicID', async (req, res) => {
  var fieldNames = req.params.fieldNames;
  var publicID = req.params.publicID;
  var tablename = req.params.tablename;
  var returnObj = {};
  var resultData = {};
  var params = [];

  if (publicID == '0') {
  // ADD new company record
    var resultData = { "ID": null, "publicID": null, "CompanyName": "New Company", "Active": true };
    resultData.Documents = [];

  } else {
    var data = await db.getItem(tablename, fieldNames, publicID);
    resultData = data[0];
    resultData.Documents = [];
    params.push({ 'name': 'Company_ID', 'dataType': sql.BigInt, 'value': resultData.ID });
    // 1. get company's documents
    var documents = await db.execute("select * from Documents where Company_ID=@Company_ID", params);
    for (var doc of documents) {
      var formattedDocument = doc;
      formattedDocument.Layouts = [{ pageNo:1, fieldArray:[] }, { pageNo:2, fieldArray:[] }, { pageNo:3, fieldArray:[] }, { pageNo:4, fieldArray:[] }, { pageNo:5, fieldArray:[] }, { pageNo:6, fieldArray:[] }, { pageNo:7, fieldArray:[] }];
// 2. get document's layouts
      params.push({ 'name': 'Document_ID', 'dataType': sql.BigInt, 'value': doc.ID });
      var layouts = await db.execute("select * from Layouts where Document_ID=@Document_ID", params);
      for (var layout of layouts) {
        if ( layout.Parent_ID == null ) {
          layout.tabularFieldArray = [];
          if ( layout.Output_DataType == '21' ) {
            // tabular data
            params.push({ 'name': 'Parent_ID', 'dataType': sql.BigInt, 'value': layout.ID });
            var tabularData = await db.execute("select * from Layouts where Document_ID=@Document_ID AND Parent_ID=@Parent_ID", params);
            for (var tabular of tabularData) {
              layout.tabularFieldArray.push(tabular);
            };
          };
          formattedDocument.Layouts[layout.PageNo-1].fieldArray.push(layout);
        } ;
      };
      resultData.Documents.push(formattedDocument);
    };
  }

  // get parameters
  params.push({ 'name': 'Active', 'dataType': sql.Bit, 'value': true });
  var lookUpData = await db.execute("select * from LookUpTables where Active=@Active", params);
  // final object for sending out
  returnObj.Data = resultData;
  returnObj.LookUpData = lookUpData;
  console.log("/layout/getitem ==>", returnObj);
  return res.status(200).send(returnObj);
});

app.post('/savelayout/:tablename/', async (req, res) => {
  var tablename = req.params.tablename;
  var body = req.body;
  var reply = null;
  var params = []
  var returnObj = {};
  var resultData = {};
  var companyID, docID, parentID = null;
  var documents = body.Data.Documents;

  if (body.Data.ID == null) {
// add into Company table
    body.Data.Active = true;
    console.log("adding company profile", body.Data, "into", tablename);
    reply = await db.addItem(tablename, body.Data);
    console.log("result from server", reply)
    if (reply) {
      companyID = reply[0].ID;
      var params = [];
      params.push({ 'name': 'ID', 'dataType': sql.BigInt, 'value': companyID});
      result = await db.execute("SELECT * FROM Company WHERE ID=@ID", params);
      console.log("result new company", result);
      resultData = result[0];
      resultData.Documents = [];
      returnObj.Data = resultData;
    };
  } else {
// update into Company table
    console.log("updating company profile", body.Data, "into", tablename);
    reply = await db.updateItem(tablename, body.Data);
  };
// loop documents array
  console.log("documents array", documents);
  for (var doc of documents) {

    if (!doc.publicID && doc.ShowDocument) {
  // document publicID null add into Documents table
      if (!doc.Company_ID && companyID) doc.Company_ID = companyID;
      console.log("adding document", doc, "into Documents");
      reply = await db.addItem('Documents', doc);
      if (reply) docID = reply[0].ID;
    } else if (doc.publicID && doc.ShowDocument) {
  // document publicID available update into Document table
      console.log("updating document", doc, "into Documents");
      reply = await db.updateItem('Documents', doc);
    } else if (doc.publicID && !doc.ShowDocument) {
      var params = [];
      params.push({ 'name': 'Document_ID', 'dataType': sql.BigInt, 'value': doc.ID});
      var layoutResult = await db.execute("DELETE FROM Layouts WHERE Document_ID=@Document_ID", params);

      reply = await db.deleteItem('Documents', doc.publicID);
    };

    if (doc.ShowDocument) {
  // loop 7 pages when document marked as available
      reply = await processLayout(doc, docID);
    };

  if (reply) {
    returnObj.Message = "Success"
  };
};
  return res.status(200).send(returnObj);
})



app.post('/layout/deleteitem/:tablename/:id', async (req, res) => {
  var tablename = req.params.tablename;
  var id = req.params.id;
  var body = req.body;
  var params = [];
  var returnObj = {};
  //comments
  if (req.params.tablename == 'Company') {
    params.push({ 'name': 'Company_ID', 'dataType': sql.BigInt, 'value': body.ID});
    var docResult = await db.execute("SELECT ID FROM Documents WHERE Company_ID=@Company_ID", params);
    // loop Documents result and delete
    if (docResult) {
      for (var doc of docResult) {
        params.push({ 'name': 'Document_ID', 'dataType': sql.BigInt, 'value': doc.ID});
        var layoutResult = await db.execute("DELETE FROM Layouts WHERE Document_ID=@Document_ID", params);
      };
    };
    var data = await db.execute("DELETE FROM Documents WHERE Company_ID=@Company_ID", params);
    console.log("deleting Company", id);
    var data = await db.deleteItem('Company', id);
  }

  if(data) {
    returnObj.Message = "Success";
  };
  console.log("/layout/deleteitem", data);
  return res.status(200).send(returnObj);
});

app.get('/lookuptables/getall/:tablename/:page/:pagesize/:sortby/:sortorder/:showAll/:lookuptableType/:searchText?', async (req, res) => {
  var sortorder = false
  if (req.params.sortorder == 'true') sortorder = true;
  var whereFieldNames = [];
  var searchFieldNames = [];
  searchFieldNames.push('Description');
  whereFieldNames.push({ 'name': 'TableType', 'dataType': sql.Int, 'value': req.params.lookuptableType, 'compare': '=' });
  if (Number(req.params.showAll) == 1) whereFieldNames.push({ 'name': 'Active', 'dataType': sql.Bit, 'value': 1, 'compare': '=' });
  var data = await db.getAll(req.params.tablename, req.params.page, req.params.pagesize, req.params.sortby, sortorder, req.params.showAll, whereFieldNames, req.params.searchText, searchFieldNames);
  var returnObj = {};
  returnObj.Data = data;
  returnObj.RecordCount = 0;
  if (data && data.length > 0) {
    returnObj.RecordCount = data[0].overall_count;
  }
  console.log("/lookuptables/getAll ==>", returnObj);
  return res.status(200).send(returnObj);
});

app.get('/lookuptables/getitem/:tablename/:fieldNames/:publicID', async (req, res) => {
  var fieldNames = req.params.fieldNames;
  var publicID = req.params.publicID;
  var tablename = req.params.tablename;
  var returnObj = {};
  var data = await db.getItem(tablename, fieldNames, publicID);
  returnObj.Data = data[0];

  console.log("/lookuptables/getitem ==>", returnObj);
  return res.status(200).send(returnObj);
});

app.post('/savelookuptables/:tablename/', async (req, res) => {
  var tablename = req.params.tablename;
  var body = req.body;
  var reply = null;
  var returnObj = {};
  reply = await db.updateItem(tablename, body.Data);
  console.log("reply from update", reply);
  if (reply) {
    returnObj = { "Message": "Success"};
  }
  return res.status(200).send(returnObj);
})

var processLayout = async function(doc, docID) {
  var parentID = null;

  for (var page of doc.Layouts) {
    for (var layout of page.fieldArray) {
      if (!layout.publicID && layout.ShowLayout) {
// layout publicID null add into Layouts table
        if (!layout.Document_ID) layout.Document_ID = docID;
        layout.PageNo = page.pageNo;
        console.log("adding layout", layout, "into Layouts");
        reply = await db.addItem('Layouts', layout);
        if (reply) parentID = reply[0].ID;
      } else if (layout.publicID && layout.ShowLayout) {
// layout publicID available update into Layouts table
        console.log("updating layout", layout, "into Layouts");
        reply = await db.updateItem('Layouts', layout);
      } else if (layout.publicID && !layout.ShowLayout) {
// layout publicID available and marked as delete
        var params = [];
        params.push({ 'name': 'Parent_ID', 'dataType': sql.BigInt, 'value': layout.ID});
        var layoutResult = await db.execute("DELETE FROM Layouts WHERE Parent_ID=@Parent_ID", params);
        console.log("deleting layout", layout);
        reply = await db.deleteItem('Layouts', layout.publicID);
      };

      if (layout.ShowLayout) {
        for (var tabular of layout.tabularFieldArray) {
          if (!tabular.Parent_ID) {
            tabular.Parent_ID = parentID;
            tabular.Document_ID = layout.Document_ID;
          }
          if (!tabular.publicID && tabular.ShowLayout) {
            console.log("adding tabular", tabular, "into Layouts");
            reply = await db.addItem('Layouts', tabular);
          } else if (tabular.publicID && tabular.ShowLayout) {
            console.log("updating tabular", tabular, "into Layouts");
            reply = await db.updateItem('Layouts', tabular);
          } else if (tabular.publicID && !tabular.ShowLayout) {
            console.log("deleting tabular", tabular);
            reply = await db.deleteItem('Layouts', tabular.publicID);
          };
        };
      };
    };
  };
  return reply;
}

module.exports = app.listen(port, () => console.log(`Example app listening on port ${port}!`))
