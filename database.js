
var jsonfile = require('jsonfile');
var repository = require('./sqlRepositoryRunTime');
const sql = require('mssql');
const _ = require('lodash');

module.exports = {
 getAll: async function (tablename, page, pagesize, sortby, sortorder, showAll, whereFieldNames, searchText, searchFieldNames) {
   try {
     var result = await repository.getPaging(tablename, "*", page, pagesize, sortby, sortorder, whereFieldNames, searchText,searchFieldNames);
     console.log("result:", result);
     if (result) {
        return result.recordsets[0];
     }
     return null;
   }
   catch (err) {
     console.log("getimage Error:", err.message);
     return({ 'err': err.message })
   }
 },

 execute: async function (query, parameter) {
   try {
     var result = await repository.execute(query, parameter);
     console.log("result:", result.recordsets[0]);
     if (result) {
        return result.recordsets[0];
     }
     return null;
   }
   catch (err) {
     console.log("getimage Error:", err.message);
     return({ 'err': err.message })
   }
 },

 getItem: async function (tablename, fieldNames, publicID) {
   try {
     var result = await repository.getSingle(tablename, fieldNames, publicID);
     if (result) {
        return result.recordsets[0];
     }
     return null;
   }
   catch (err) {
     console.log("getimage Error:", err.message);
     return({ 'err': err.message })
   }
 },

 addItem: async function(tablename, object) {
   try {
     var result = await repository.add(tablename, object);
     if (result) {

          return result.recordsets[0];

     }
   }
   catch (err) {
     console.log("getimage Error:", err.message);
     return({ 'err': err.message })
   }
 },

 updateItem: async function(tablename, object) {
   try {
     var result = await repository.update(tablename, object);
     if (result) {
         _.forEach(result.recordsets, function (row) {
             console.log(row);
         });
        return result.recordsets[0];
     }
   }
   catch (err) {
     console.log("getimage Error:", err.message);
     return({ 'err': err.message })
   }
 },

 deleteItem: async function(tablename, publicID) {
   try {
     var result = await repository.delete(tablename, publicID);
     if (result) {

          return result.recordsets[0];

     }
   }
   catch (err) {
     console.log("getimage Error:", err.message);
     return({ 'err': err.message })
   }
 }

};
