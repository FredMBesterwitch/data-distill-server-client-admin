var chai = require('chai');
var should = chai.should();
var expect = chai.expect;
var chaiHttp = require("chai-http");
var request = require('request');
var server = require("../../server");
var assert = require('assert');
chai.use(chaiHttp);

var expected = {
  "ItemCode": "2",
  "CompanyName": "CompanyName - 02",
  "available": true,
  "Documents": []
};

describe('status and content', function() {
  describe('main page', function() {
    it('Main page content', function(done) {
        request('http://localhost:8000/' , function(error, response, body) {
            expect(body).to.equal('Hello World');
            done();
        });
    });
  });

  describe('server API getname', function() {
    it('server getname', function(done) {
        chai.request(server)
          .get('/getname')
          .end((err, res) => {
            expect(res).to.have.status(200);
            expect(res.text).to.equals('happy');
            done();
          });
    });
  });

  describe('server API getitem', function() {
    it('return data if found', function(done) {
        chai.request(server)
          .get('/layout/getitem/tablename/2')
          .end((err, res) => {
            expect(res).to.have.status(200);
            expect(res.body).to.deep.equal(expected);
            done();
          });
    });

    it('return data if not found', function(done) {
        chai.request(server)
          .get('/layout/getitem/tablename/5550')
          .end((err, res) => {
            expect(res).to.have.status(200);
            expect(res.body).to.deep.equal({ 'data': false });
            done();
          });
    });
  });

  describe('server API savelayout', function() {
    it('post data', function(done) {
      var item = {

        "Data": {
          "ItemCode": "6",
          "CompanyName": "CompanyName - 06",
          "available": true,
          "Documents": []
        }

      }
      chai.request(server)
        .post('/savelayout/tablename')
        .send(item)
        .end((err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });
  });

});
